VERSION := $(shell jq -r .version manifest.json)
FOLDER := $(shell basename $$PWD)

.PHONY: zip
zip: bluejeans-webrtc-$(VERSION).zip

.PHONY: ext
ext: bluejeans-webrtc-$(VERSION).crx

.PHONY: clean
clean:
	rm -f *.crx *.zip

bluejeans-webrtc-$(VERSION).crx: manifest.json background.js
	zip $@ $+

bluejeans-webrtc-$(VERSION).zip: manifest.json background.js
	cd ..; zip $(FOLDER)/$@ $$(for file in $+; do echo "$(FOLDER)/$$file"; done)
