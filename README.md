# Google Chrome Extension - BlueJeans WebRTC

This Google Chrome extension changes BlueJeans URLs so that they use the WebRTC
API.

This is handy for platforms where BlueJeans doesn't provide a plugin for instance
Linux distributions without RPMs (Debian, Ubuntu, Arch, etc) or even on
platforms where the plugin exists but is not wanted.

## Publishing

Go to the Google Chrome WebStore page: https://chrome.google.com/webstore/developer/dashboard

Click "Add new item" and upload the zip file (generate it with `make zip`).

https://support.google.com/chrome/a/answer/2714278?hl=en
