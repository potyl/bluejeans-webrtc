// Webcall urls (ex: "/111", "/123456", "/123456/7890")
const WEBCALL_REGEXP = /^\/[0-9\/]+$/i;

chrome.webRequest.onBeforeRequest.addListener(
    function(details) {
        url = new URL(details.url);

        if (!url.pathname.match(WEBCALL_REGEXP) || url.pathname.endsWith("/webrtc")) {
            return;
        }

        url.pathname += '/webrtc';

        return {
            redirectUrl: url.toString(),
        };
    },
    {
        urls: [
            "*://bluejeans.com/*",
            "*://*.bluejeans.com/*",
        ],
        types: [
            "image",
            "main_frame",
            "object",
            "other",
            "script",
            "stylesheet",
            "sub_frame",
            "xmlhttprequest",
        ]
    },
    ["blocking"]
);
